using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    GameObject m_player;
    public GameObject m_punchSound;
    public GameObject m_defeatSound;
    public Animator m_anim;
    public float m_speed = 6;
    public float m_life = 100;
    public float m_distance;
    public float m_offset;
    public float m_time = 5f;
    public static bool m_hitTime = false;

    // Start is called before the first frame update
    void Start()
    {
        m_player = GameObject.Find("Kunio");
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(m_player.transform.position, this.transform.position + new Vector3(0, m_offset, 0)) > m_distance && m_life > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_player.transform.position, m_speed);
        }
        else
        {
            Punch();
            SetLife();
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(0, m_offset, 0), m_distance);
    }

    public void Kill()
    {
        Instantiate(m_defeatSound);
        Kunio.m_score += 100;
        Destroy(gameObject);
    }

    public void Punch()
    {
        GetComponent<Rigidbody2D>().velocity = Vector3.zero;

        if (!m_anim.GetCurrentAnimatorStateInfo(0).IsName("gpunch") &&
           !m_anim.GetCurrentAnimatorStateInfo(0).IsName("edefeat") &&
           !m_anim.GetCurrentAnimatorStateInfo(0).IsName("gpunched"))
        {
            m_anim.SetTrigger("gpunch");
        }
        else if (m_anim.GetCurrentAnimatorStateInfo(0).IsName("gpunch"))
        {
            float animTime = m_anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (animTime > 1.70 && animTime < 1.71) m_hitTime = true;
            else m_hitTime = false;
        }
        else m_hitTime = false;
    }

    public void SetLife()
    {
        if (Kunio.m_hitTime)
        {
            m_life--;
            if (m_life <= 0)
            {
                m_anim.SetTrigger("edefeat");
                Invoke("Kill", m_time);
            }
            else m_anim.SetTrigger("gpunched");
            Instantiate(m_punchSound);
        }
    }
}
