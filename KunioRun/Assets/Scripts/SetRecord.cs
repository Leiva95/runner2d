using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetRecord : MonoBehaviour
{
    public Text m_record;

    // Start is called before the first frame update
    void Start()
    {
        m_record.text = "RECORD   " + PlayerPrefs.GetInt("record", 0).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Kunio.m_score > PlayerPrefs.GetInt("record")) PlayerPrefs.SetInt("record", (int)Kunio.m_score);
        m_record.text = "RECORD   " + PlayerPrefs.GetInt("record", 0).ToString();
    }
}
