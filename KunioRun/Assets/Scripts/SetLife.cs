using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetLife : MonoBehaviour
{
    public Text m_kunioLife;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_kunioLife.text = "KUNIO   " + new string('|', (int)Kunio.m_life);
    }
}
