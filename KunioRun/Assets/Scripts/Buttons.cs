using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public GameObject m_selectSound;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Restart()
    {
        Instantiate(m_selectSound);
        Kunio.m_life = 10;
        Kunio.m_score = 0;
        SceneManager.LoadScene(0);
    }

    public void Exit()
    {
        Instantiate(m_selectSound);
        Application.Quit();
    }
}
