using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public float m_speed = -2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x <= -22)
            Destroy(gameObject);
        else
            GetComponent<Rigidbody2D>().velocity = new Vector3(m_speed, 0, 0);
    }
}
