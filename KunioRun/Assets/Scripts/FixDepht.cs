using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixDepht : MonoBehaviour
{
    public bool m_fixFrame = true;
    SpriteRenderer m_sr;

    // Start is called before the first frame update
    void Start()
    {
        m_sr = GetComponent<SpriteRenderer>();
        m_sr.sortingLayerName = "Kunio";
        m_sr.sortingOrder = Mathf.RoundToInt(-transform.position.y * 100);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_fixFrame) m_sr.sortingOrder = Mathf.RoundToInt(-transform.position.y * 100);
    }
}
