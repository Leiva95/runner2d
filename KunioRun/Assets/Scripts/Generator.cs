using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    public GameObject m_obj;
    public float m_time = 6f;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(m_obj, transform.position, Quaternion.identity);
        Invoke("Start", m_time);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
