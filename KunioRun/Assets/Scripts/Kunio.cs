using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kunio : MonoBehaviour
{
    GameObject m_enem;
    public GameObject m_obj;
    public GameObject m_airSound;
    public GameObject m_punchSound;
    public GameObject m_defeatSound;
    public GameObject m_gameoverMusic;
    public Animator m_anim;
    public float m_offset;
    public float m_distance;
    public float m_speed = 3;
    public static float m_score = 0;
    public static float m_life = 10;
    public static bool m_hitTime = false;

    // Start is called before the first frame update
    void Start()
    {
        m_anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        m_enem = GameObject.FindGameObjectWithTag("Enemy");
        SetLife();
        PlayerMovement();
        Punch();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(0, m_offset, 0), m_distance);
    }

    public void PlayerMovement()
    {
        if (m_life < 1) 
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
        else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
            GetComponent<Rigidbody2D>().velocity = new Vector3(m_speed * Mathf.Sqrt(2) / 2, m_speed * Mathf.Sqrt(2) / 2, 0);
        else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A))
            GetComponent<Rigidbody2D>().velocity = new Vector3(-m_speed * Mathf.Sqrt(2) / 2, m_speed * Mathf.Sqrt(2) / 2, 0);
        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
            GetComponent<Rigidbody2D>().velocity = new Vector3(m_speed * Mathf.Sqrt(2) / 2, -m_speed * Mathf.Sqrt(2) / 2, 0);
        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A))
            GetComponent<Rigidbody2D>().velocity = new Vector3(-m_speed * Mathf.Sqrt(2) / 2, -m_speed * Mathf.Sqrt(2) / 2, 0);
        else if (Input.GetKey(KeyCode.W))
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, m_speed * Mathf.Sqrt(2) / 2, 0);
        else if (Input.GetKey(KeyCode.S))
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, -m_speed * Mathf.Sqrt(2) / 2, 0);
        else if (Input.GetKey(KeyCode.D))
            GetComponent<Rigidbody2D>().velocity = new Vector3(m_speed * Mathf.Sqrt(2) / 2, 0, 0);
        else if (Input.GetKey(KeyCode.A))
            GetComponent<Rigidbody2D>().velocity = new Vector3(-m_speed * Mathf.Sqrt(2) / 2, 0, 0);
        else 
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
    }

    public void SetLife()
    {
        if (Vector2.Distance(m_enem.transform.position, this.transform.position + new Vector3(0, m_offset, 0)) < m_distance && m_life > 0)
        {
            if (Enemy.m_hitTime)
            {
                m_life--;
                if (m_life <= 0)
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, -10);
                    GameObject music = GameObject.Find("StreetTheme");
                    Destroy(music);
                    m_anim.SetTrigger("defeat");
                    Instantiate(m_defeatSound);
                    Instantiate(m_obj);
                    Invoke("MusicStart", 2);
                }
                else m_anim.SetTrigger("hitted");
                Instantiate(m_punchSound);
            }
        }
    }

    public void Punch()
    {
        if (Input.GetKeyDown(KeyCode.K) && m_life > 0 &&
            !m_anim.GetCurrentAnimatorStateInfo(0).IsName("punching") &&
            !m_anim.GetCurrentAnimatorStateInfo(0).IsName("punched") &&
            !m_anim.GetCurrentAnimatorStateInfo(0).IsName("defeated"))
        {
            Instantiate(m_airSound);
            m_anim.SetTrigger("hit");
            m_hitTime = true;
        }
        else m_hitTime = false;
    }

    public void MusicStart()
    {
        Instantiate(m_gameoverMusic);
    }
}
